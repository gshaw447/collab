package com.example.collab

import android.app.Activity.RESULT_OK
import android.bluetooth.BluetoothClass.Service.AUDIO
import android.content.Intent
import android.media.AudioManager
import android.media.MediaPlayer
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.EditText
import android.widget.TextView
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.Toast
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.google.firebase.storage.UploadTask


class ArtistPage : AppCompatActivity() {


    var fStore:FirebaseFirestore? = null
    var fAuth:FirebaseAuth? = null


    //music upload
    val AUDIO : Int = 0
    lateinit var uri : Uri
    lateinit var mStorage : StorageReference

    private lateinit var mp: MediaPlayer





    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_artist_page)
        var name:TextView = findViewById(R.id.profName1)
        var loc:TextView = findViewById(R.id.profLoc1)
        var messages:TextView = findViewById(R.id.messagesText1)

        //music upload
        val playBtn = findViewById<View>(R.id.playMusic1) as Button
        val pauseBtn = findViewById<View>(R.id.pauseMusic1) as Button
        mStorage = FirebaseStorage.getInstance().getReference("Uploads")
        var pause: Boolean = false
        val mediaPlayer = MediaPlayer()
        var position: Int = 0

        fStore = FirebaseFirestore.getInstance()
        fAuth = FirebaseAuth.getInstance()
        fStore?.collection("currentArtist")?.get()?.addOnSuccessListener { result ->
            for(document in result)
            {
                name.text = document.get("currArtist").toString()
                loc.text = "Location: " +document.get("location").toString()
                fStore?.collection("users")?.get()?.addOnSuccessListener { result ->
                    for(document in result)
                    {
                        if((document.get("username").toString()==name.text)&&(document.get("message")!=""))
                        {
                            messages.text = document.get("message").toString()
                            loc.text = loc.text as String +"\nGenre:" +document.get("genre").toString()
                        }
                    }
                }
            }
        }
        var messageBox:EditText = findViewById(R.id.messageBox1)
        var subButton:Button = findViewById(R.id.subMessage1)
        subButton.setOnClickListener {
            fStore?.collection("users")
                ?.get()
                ?.addOnSuccessListener { result ->
                    for (document in result) {
                        var newArtistDoc:Artist = Artist()
                        var compName:String = document.get("username").toString()
                        if(compName==name.text)
                        {
                            val map:HashMap<String,String> = HashMap()
                            var s:String = document.get("message").toString()
                            map["name"] = document.get("name").toString()
                            map["username"] = document.get("username").toString()
                            map["image"] =document.get("image").toString()
                            map["description"]=document.get("description").toString()
                            map["genre"]=document.get("genre").toString()
                            Log.e("user", fAuth?.currentUser?.email)
                            map["message"] = s+"\n"+fAuth?.currentUser?.email.toString()+": "+messageBox.text
                            fStore?.collection("users")?.document(document.id)?.set(map)
                            messages.text = document.get("message")?.toString()
                            finish()
                            startActivity(getIntent())
                        }
                        newArtistDoc.location = document.get("description").toString()
                        //newArtistDoc.image = document.get("image").toString()

                    }
                }
        }

        playBtn.setOnClickListener(View.OnClickListener {

            uri = Uri.parse("https://firebasestorage.googleapis.com/v0/b/collab-3c362.appspot.com/o/Uploads%2Fraw%3A%2Fstorage%2Femulated%2F0%2FDownload%2Fsong.mp3?alt=media&token=b0c77714-2b5f-4cbe-bd45-5f3f49388adc")


            //YET ANOTHER METHOD
            //mAuth.signInWithEmailAndPassword("test@gmail.com", "123123")
            //mStorage.child("Download/song.mp3").downloadUrl.addOnSuccessListener({

            if(pause == false) {
                mediaPlayer.setDataSource(uri.toString())
                mediaPlayer.setOnPreparedListener { player ->
                    player.start()
                }
                mediaPlayer.prepareAsync()
            }
            else{
                mediaPlayer.seekTo(position)
                mediaPlayer.start()

            }

        })

        pauseBtn.setOnClickListener {
            if(mediaPlayer.isPlaying){
                mediaPlayer.pause()
                position = mediaPlayer.currentPosition
                pause = true

            }
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        val uriTxt = findViewById<View>(R.id.uriTxt1) as TextView
        if (resultCode == RESULT_OK) {
            if (requestCode == AUDIO) {
                uri = data!!.data
                uriTxt.text = uri.toString()
                upload ()
            }
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

    private fun upload() {

        var mReference = mStorage.child(uri.lastPathSegment)

        try {
            mReference.putFile(uri).addOnSuccessListener {
                    taskSnapshot: UploadTask.TaskSnapshot? -> var url = taskSnapshot
                val dwnTxt = findViewById<View>(R.id.dwnTxt1) as TextView
                dwnTxt.text = url.toString()
                Toast.makeText(this, "Successfully Uploaded :)", Toast.LENGTH_LONG).show()
            }
        }catch (e: Exception) {
            Toast.makeText(this, e.toString(), Toast.LENGTH_LONG).show()
        }

    }
}
