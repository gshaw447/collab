package com.example.collab.Activities

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import com.example.collab.Classes.Event
import com.example.collab.R
import com.example.collab.Classes.*
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore

class CollabPage : AppCompatActivity() {
    var fStore: FirebaseFirestore? = null
    var fAuth: FirebaseAuth? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_collab_page)
        var name: TextView = findViewById(R.id.collabName)
        var loc: TextView = findViewById(R.id.collabLoc)
        var message: TextView = findViewById(R.id.messagesCollab)

        fStore = FirebaseFirestore.getInstance()
        fAuth = FirebaseAuth.getInstance()
        fStore?.collection("currentCollab")?.get()?.addOnSuccessListener { result ->
            for(document in result)
            {
                name.text = document.get("user").toString()
                loc.text = "Description: " +document.get("description").toString()
                fStore?.collection("users")?.get()?.addOnSuccessListener { result ->
                    for(document in result)
                    {
                        if((document.get("name").toString()==name.text))
                            {
                                fStore?.collection("collabs")?.get()?.addOnSuccessListener { result ->
                                    for (doc in result) {
                                        if ((doc.get("collabmessage") != "") && (doc.get("user") == name.text)) {
                                            message.text = doc.get("collabmessage").toString()
                                            loc.text =
                                                loc.text as String //+"\nGenre:" +document.get("genre").toString()
                                        }
                                    }
                                }
                            }
                    }
                }
            }
        }
        var messageBox: EditText = findViewById(R.id.messageBoxCollab)
        var subButton: Button = findViewById(R.id.subMessageCollab)
        subButton.setOnClickListener {
            fStore?.collection("collabs")
                ?.get()
                ?.addOnSuccessListener { result ->
                    for (document in result) {
                        var newCollabDoc: Collab = Collab()
                        var compName:String = document.get("user").toString()
                        if((compName==name.text))
                        {
                            val map:HashMap<String,String> = HashMap()
                            var s:String = document.get("collabmessage").toString()
                            map["user"] = document.get("user").toString()
                            map["expr"] = document.get("expr").toString()
                            map["collabmessage"] = document.get("collabmessage").toString()+"\n"+fAuth?.currentUser?.email.toString()+": "+messageBox.text

                            fStore?.collection("collabs")?.document(document.id)?.set(map)
                            message.text = document.get("collabmessage")?.toString()
                            finish()
                            startActivity(getIntent())
                        }
                        //newEventDoc.location = document.get("description").toString()
                        //newArtistDoc.image = document.get("image").toString()

                    }
                }
        }



    }
}
