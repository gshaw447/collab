package com.example.collab

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.EditText
import android.widget.TextView
import android.util.Log
import android.widget.Button
import com.example.collab.Classes.Event
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.auth.FirebaseAuth
class EventPage : AppCompatActivity() {
    var fStore:FirebaseFirestore? = null
    var fAuth:FirebaseAuth? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_event_page)
        var name:TextView = findViewById(R.id.eventName)
        var loc:TextView = findViewById(R.id.eventLoc)
        //var messages:TextView = findViewById(R.id.messageEvent)
        var message:TextView = findViewById(R.id.messagesText)

        fStore = FirebaseFirestore.getInstance()
        fAuth = FirebaseAuth.getInstance()
        fStore?.collection("currentEvent")?.get()?.addOnSuccessListener { result ->
            for(document in result)
            {
                name.text = document.get("user").toString()
                loc.text = "Description: " +document.get("description").toString()
                fStore?.collection("users")?.get()?.addOnSuccessListener { result ->
                    for(document in result)
                    {
                        if(document.get("name").toString()==name.text)
                        {
                            fStore?.collection("events")?.get()?.addOnSuccessListener {
                                result ->
                                for(doc in result)
                                {
                                    if ((doc.get("eventmessage")!="")&&(doc.get("user")==name.text))
                                    {
                                        message.text = doc.get("eventmessage").toString()
                                        loc.text = loc.text as String //+"\nGenre:" +document.get("genre").toString()
                                    }
                                }
                            }

                        }
                    }
                }
            }
        }
        var messageBox:EditText = findViewById(R.id.messageEvent)
        var subButton:Button = findViewById(R.id.subMessageEvent)
        subButton.setOnClickListener {
            fStore?.collection("events")
                ?.get()
                ?.addOnSuccessListener { result ->
                    for (document in result) {
                        var compName:String = document.get("user").toString()
                        if((compName==name.text)&&(document.get("eventmessage")!=""))
                        {
                            val map:HashMap<String,String> = HashMap()
                            map["eventmessage"] = document.get("eventmessage")?.toString()+"\n"+fAuth?.currentUser?.email.toString()+": "+messageBox.text
                            map["user"] = document.get("user").toString()
                            map["expr"] = document.get("expr").toString()
                            map["description"] = document.get("description").toString()
                            fStore?.collection("events")?.document(document.id)?.set(map)
                            message.text = document.get("eventmessage")?.toString()+"\n"+fAuth?.currentUser?.email.toString()+messageBox.text
                            finish()
                            startActivity(getIntent())
                        }
                        //newEventDoc.location = document.get("description").toString()
                        //newArtistDoc.image = document.get("image").toString()

                    }
                }
        }



    }
}
