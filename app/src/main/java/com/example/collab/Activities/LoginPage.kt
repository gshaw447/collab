package com.example.collab

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.text.TextUtils
import android.util.Patterns
import android.view.View
import java.util.*
import com.firebase.client.Firebase
import android.widget.EditText
import android.widget.TextView
import android.widget.Button
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.OnSuccessListener
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.FirebaseApp
import com.google.firebase.FirebaseApp.initializeApp
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.FirebaseFirestore
import android.util.Log
import com.example.collab.Activities.UploadMusic

class LoginPage : AppCompatActivity() {

    private var email: EditText? = null
    private var password: EditText? = null
    private var authListener: FirebaseAuth.AuthStateListener? = null
    private var fStore: FirebaseFirestore? = null
    private var fAuth:FirebaseAuth? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        initializeApp(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login_page)
        email = findViewById(R.id.email)
        password = findViewById(R.id.password)
        var signIn: Button  = findViewById(R.id.signIn)
        var signUp: Button  = findViewById(R.id.signUp)
        val uploadButton:Button = findViewById(R.id.uploadButton)
        fAuth = FirebaseAuth.getInstance()
        fStore = FirebaseFirestore.getInstance()
        authListener = FirebaseAuth.AuthStateListener {
            if (fAuth?.currentUser != null) {
                finish()//startActivity(Intent(this@LoginPage, MainActivity::class.java))

            }
        }
        //fAuth?.signOut() forces user to log in each time they open the app, used to test sign in
        signIn.setOnClickListener(View.OnClickListener { signIn() })
        signUp.setOnClickListener(View.OnClickListener { register() })
        uploadButton.setOnClickListener(View.OnClickListener {  startActivity(Intent(this@LoginPage, UploadMusic::class.java)) })

            //setContentView(R.layout.activity_upload)

    }

    override fun onStart() {
        super.onStart()
        this.authListener?.let { fAuth?.addAuthStateListener(it) }
    }

    private fun signIn() {
        val e = email?.getText().toString().trim { it <= ' ' }
        val pass = password?.getText().toString()
        if (TextUtils.isEmpty(e) || TextUtils.isEmpty(pass)) {
            val err: TextView = this.findViewById(R.id.error)
            err.setText("One or more fields is empty")
        } else {
            fAuth?.signInWithEmailAndPassword(e, pass)?.addOnCompleteListener { task ->
                if (task.isSuccessful == false) {
                    val err: TextView = findViewById(R.id.error)
                    err.setText("Sign In problem")
                }
                else {
                    finish()
                }
            }

        }
    }

    private fun register() {
        email = findViewById(R.id.emailNew)
        password = findViewById(R.id.passnew)
        val e:String = email?.text.toString()
        val pass = password?.text.toString()
        if (TextUtils.isEmpty(e) || TextUtils.isEmpty(pass)) {
            val err:TextView = findViewById(R.id.error)
            err.setText("One or more fields is empty")
            return
        } else if (Patterns.EMAIL_ADDRESS.matcher(e).matches() == false) {
            val err:TextView = findViewById(R.id.error)
            err.setText("Please enter valid email")
            return
        } else if (pass.length < 8) {
            val err:TextView = findViewById(R.id.error)
            err.setText("password should be at least 8 characters")
            return
        } else {
            fAuth?.createUserWithEmailAndPassword(e, pass)?.addOnSuccessListener {
                val err:TextView = findViewById(R.id.error)
                val usrName:EditText = findViewById(R.id.username)
                val genre:EditText = findViewById(R.id.genre)
                val descr:EditText = findViewById(R.id.descr)
                err.setText("Succsfully created account")
                val users = HashMap<String, String>()
                val nameString:String = usrName?.text.toString()
                val genreString:String = genre?.text.toString()
                val descrString:String = descr?.text.toString()
                users["name"] = e
                users["username"] = nameString
                users["description"] = descrString
                users["genre"] = genreString
                users["image"] = "image placeholder"
                users["message"] = ""

                fStore?.collection("users")?.add(users)
                fAuth?.signInWithEmailAndPassword(e,pass)
                signIn()
                Log.e("signin", "success")
            }

        }

        return


    }
}
