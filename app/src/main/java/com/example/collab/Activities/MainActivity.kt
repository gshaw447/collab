package com.example.collab

import android.annotation.SuppressLint
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.app.FragmentStatePagerAdapter
import android.view.Menu
import android.view.MenuItem
import android.widget.Button
import kotlinx.android.synthetic.main.activity_main.*
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.auth.FirebaseAuth
import android.content.ClipData.Item
import android.util.Log
import com.google.firebase.firestore.QuerySnapshot

class MainActivity : AppCompatActivity() {
    private var fAuth:FirebaseAuth? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        fAuth = FirebaseAuth.getInstance()
        setContentView(R.layout.activity_main)

        // TODO: Check if authenticated before starting log in activity

        startActivity(Intent(this@MainActivity, LoginPage::class.java))

        // Make tab layout
        val fragmentAdapter = MyPagerAdapter(supportFragmentManager, this)
        frag_placeholder.adapter = fragmentAdapter
        frag_placeholder.offscreenPageLimit = 2
        tabs.setupWithViewPager(frag_placeholder)
    }

    // Add menu at top
    @SuppressLint("NewApi")
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    // Got to new post when selected
    @SuppressLint("NewApi")
    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> {
                this.finish()
                return true
            }
            R.id.new_post -> {
                // Start new post activity
                startActivity(Intent(this@MainActivity, MakePost::class.java))

                return false
            }
            R.id.logoutbutton -> {
                // Log out
                fAuth?.signOut()
                startActivity(Intent(this@MainActivity, LoginPage::class.java))

                return false
            }
            R.id.profile_button -> {
                // Log out
                fAuth?.signOut()
                startActivity(Intent(this@MainActivity, ProfileActivity::class.java))

                return false
            }
        }
        return super.onOptionsItemSelected(item)
    }

    // Class to support tabbed fragments
    class MyPagerAdapter(fm: FragmentManager, context: Context) : FragmentStatePagerAdapter(fm) {
        val c = context

        override fun getItem(position: Int): Fragment {
            return when (position) {
                0 -> {
                    // List of artist profiles
                    return ArtistFragment(c)
                }
                1 -> {
                    // List of events looking for artists
                    return EventFragment(c)
                }
                else -> {
                    // List of collabs
                    return CollabFragment(c)
                }
            }
        }

        override fun getCount(): Int {
            return 3
        }

        override fun getPageTitle(position: Int): CharSequence {
            return when (position) {
                0 -> "Artists"
                1 -> "Events"
                else -> "Collabs"
            }
        }
    }

}
