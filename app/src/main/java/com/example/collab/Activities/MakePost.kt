package com.example.collab

import android.content.Context
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import android.view.View
import com.example.collab.Fragments.NewCollabFragment
import com.example.collab.Fragments.NewEventFragment
import kotlinx.android.synthetic.main.activity_make_post.*
import kotlinx.android.synthetic.main.fragment_new_event.*

class MakePost : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_make_post)

        // Make tab layout
        val fragmentAdapter = MakePost.MyPagerAdapter(supportFragmentManager, this)
        frag_placeholder.adapter = fragmentAdapter

        tabs.setupWithViewPager(frag_placeholder)
    }

    // Class to support tabbed fragments
    class MyPagerAdapter(fm: FragmentManager, context: Context) : FragmentStatePagerAdapter(fm) {
        val c = context

        override fun getItem(position: Int): Fragment {
            return when (position) {
                0 -> {
                    // New collab post creator
                    NewCollabFragment(c)
                }
                1 -> {
                    // New event post creator
                    NewEventFragment(c)
                }
                else -> {
                    //Backup
                    NewCollabFragment(c)
                }

            }
        }

        override fun getCount(): Int {
            return 2
        }

        override fun getPageTitle(position: Int): CharSequence {
            return when (position) {
                0 -> "New Collab"
                1 -> "New Event"
                else -> {
                    return "Invalid Tab"
                }
            }
        }
    }
}
