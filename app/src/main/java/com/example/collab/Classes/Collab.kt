package com.example.collab.Classes

import com.example.collab.Artist
import java.io.Serializable

class Collab(): Serializable {
    var title = "Collab 1"
    var artist: Artist = Artist()
    var description = "Looking to collab in this way, with these requirements"
    var location = ""
    var image = ""
}