package com.example.collab.Classes

import com.example.collab.Artist
import java.io.Serializable

class Event (): Serializable {
    var title = "Event 1"
    var eventHost = "Event Host 1"
    var description = "Looking for performer at this time, in this place, with this genre"
    var location =  ""
    var image = ""
}