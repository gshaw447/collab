package com.example.collab

import android.annotation.SuppressLint
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.util.DiffUtil
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.artist_item.view.*
import kotlinx.android.synthetic.main.fragment_artist_list.*
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.QuerySnapshot
@SuppressLint("ValidFragment")
class ArtistList(context: Context, query: String): Fragment() {
    // Variables
    private var adapter = ArtistAdapter()
    private var parentContext: Context = context
    private lateinit var viewModel: ArtistViewModel
    private var queryString: String = query
    private var artistList: ArrayList<Artist> = ArrayList()
    var text:TextView? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        text = view?.findViewById(R.id.artistSearch)

         //findViews(view?.findViewById(R.id.localArtists))
        return  inflater.inflate(R.layout.fragment_artist_list, container, false)


    }
    fun findViews(v: View?) {
        try {
            if (v is ViewGroup) {
                for (i in 0 until v.childCount) {
                    val child = v.getChildAt(i)
                    // recursively call this method
                    findViews(child)
                }
            } else if (v is TextView) {
                if(v.text.contains(text as Regex))
                {
                    v.alpha = 1.0f
                }
                else
                {
                    v.alpha = 0.0f
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }
    override fun onStart() {
        super.onStart()
        localArtists.layoutManager = LinearLayoutManager(parentContext)

        viewModel = ViewModelProviders.of(this).get(ArtistViewModel::class.java)

        val observer = Observer<ArrayList<Artist>> {
            localArtists.adapter = adapter
            val result = DiffUtil.calculateDiff(object : DiffUtil.Callback() {
                override fun areItemsTheSame(p0: Int, p1: Int): Boolean {
                    return (artistList[p0].name == artistList[p1].name) and (artistList[p0].location == artistList[p1].location)
                }

                override fun getOldListSize(): Int {
                    return artistList.size
                }

                override fun getNewListSize(): Int {
                    if (it == null) {
                        return 0
                    }
                    return it.size
                }

                override fun areContentsTheSame(p0: Int, p1: Int): Boolean {
                    return artistList[p0] == artistList[p1]
                }
            })
            result.dispatchUpdatesTo(adapter)
            artistList = it ?: ArrayList()
        }

        viewModel.getArtists(queryString).observe(this, observer)
    }

    // Adapter for artist items
    inner class ArtistAdapter: RecyclerView.Adapter<ArtistAdapter.ArtistViewHolder>() {

        override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ArtistViewHolder {
            val itemView = LayoutInflater.from(p0.context).inflate(R.layout.artist_item, p0, false)
            return ArtistViewHolder(itemView)
        }

        override fun onBindViewHolder(p0: ArtistViewHolder, p1: Int) {
            val artist = artistList[p1]
            p0.name.text = artist.name
            p0.location.text = artist.location

            if(!artist.image.isEmpty()) {
                Picasso.with(this@ArtistList.parentContext).load(artist.image).into(p0.image)
            }

            p0.item.setOnClickListener {
               val fStore:FirebaseFirestore = FirebaseFirestore.getInstance()
                val map:HashMap<String,String> = HashMap<String,String>()
                map["currArtist"] = p0.name.text.toString()
               map["location"] = p0.location.text.toString()
                fStore.collection("currentArtist").document("Y6wb5F6IMTBqt0VFNvUm").set(map)
                val intent = Intent(this@ArtistList.parentContext, ArtistPage::class.java)
                intent.putExtra("ARTIST", artist)
                startActivity(intent)
            }
        }

        override fun getItemCount(): Int {
            return artistList.size
        }

        inner class ArtistViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
            var item = itemView
            var location: TextView = itemView.artistLocation
            var name: TextView = itemView.artistTitle
            var image: ImageView = itemView.artistIcon
        }
    }
}