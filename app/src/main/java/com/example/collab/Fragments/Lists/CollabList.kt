package com.example.collab.Fragments.Lists

import android.annotation.SuppressLint
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.util.DiffUtil
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.example.collab.Activities.CollabPage
import com.example.collab.Classes.Collab
import com.example.collab.ProfileActivity
import com.example.collab.R
import com.example.collab.ViewModels.CollabViewModel
import com.google.firebase.firestore.FirebaseFirestore
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.collab_item.view.*
import kotlinx.android.synthetic.main.fragment_collab_list.*

@SuppressLint("ValidFragment")
class CollabList(context: Context, query: String): Fragment() {
    // Variables
    private var adapter = CollabAdapter()
    private var parentContext: Context = context
    private lateinit var viewModel: CollabViewModel

    private var queryString: String = query
    private var collabList: ArrayList<Collab> = ArrayList()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_collab_list, container, false)
    }

    override fun onStart() {
        super.onStart()
        localCollabs.layoutManager = LinearLayoutManager(parentContext)

        viewModel = ViewModelProviders.of(this).get(CollabViewModel::class.java)

        val observer = Observer<ArrayList<Collab>> {
            localCollabs.adapter = adapter
            val result = DiffUtil.calculateDiff(object : DiffUtil.Callback() {
                override fun areItemsTheSame(p0: Int, p1: Int): Boolean {
                    return (collabList[p0].title == collabList[p1].title) and (collabList[p0].location == collabList[p1].location)
                }

                override fun getOldListSize(): Int {
                    return collabList.size
                }

                override fun getNewListSize(): Int {
                    if (it == null) {
                        return 0
                    }
                    return it.size
                }

                override fun areContentsTheSame(p0: Int, p1: Int): Boolean {
                    return collabList[p0] == collabList[p1]
                }
            })
            result.dispatchUpdatesTo(adapter)
            collabList = it ?: ArrayList()
        }

        viewModel.getCollabs(queryString).observe(this, observer)
    }

    // Addapter for artist items
    inner class CollabAdapter: RecyclerView.Adapter<CollabAdapter.CollabViewHolder>() {

        override fun onCreateViewHolder(p0: ViewGroup, p1: Int): CollabViewHolder {
            val itemView = LayoutInflater.from(p0.context).inflate(R.layout.collab_item, p0, false)
            return CollabViewHolder(itemView)
        }

        override fun onBindViewHolder(p0: CollabViewHolder, p1: Int) {
            val collab = collabList[p1]
            p0.title.text = collab.title
            p0.location.text = collab.location

            if(!collab.image.isEmpty()) {
                Picasso.with(this@CollabList.parentContext).load(collab.image).into(p0.image)
            }

            p0.item.setOnClickListener {
                val fStore: FirebaseFirestore = FirebaseFirestore.getInstance()
                val map:HashMap<String,String> = HashMap<String,String>()
                map["description"] = collab.title+"\n"+collab.description
                map["user"] = collab.artist.name
                fStore.collection("currentCollab").document("J3nLt9vR20xQNw8SBAEZ").set(map)
                val intent = Intent(this@CollabList.parentContext, CollabPage::class.java)
                intent.putExtra("COLLAB", collab)
                startActivity(intent)
            }
        }

        override fun getItemCount(): Int {
            return collabList.size
        }

        inner class CollabViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
            var item = itemView
            var location: TextView = itemView.collabLocation
            var title: TextView = itemView.eventTitle
            var image: ImageView = itemView.collabIcon
        }
    }
}