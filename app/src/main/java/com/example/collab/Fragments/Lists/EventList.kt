package com.example.collab.Fragments.Lists

import android.annotation.SuppressLint
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.util.DiffUtil
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.example.collab.Classes.Event
import com.example.collab.EventPage
import com.example.collab.R
import com.example.collab.ViewModels.EventViewModel
import com.google.firebase.firestore.FirebaseFirestore
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.event_item.view.*
import kotlinx.android.synthetic.main.fragment_event_list.*

@SuppressLint("ValidFragment")
class EventList(context: Context, query: String): Fragment() {
    // Variables
    private var adapter = EventAdapter()
    private var parentContext: Context = context
    private lateinit var viewModel: EventViewModel

    private var queryString: String = query
    private var eventList: ArrayList<Event> = ArrayList()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_event_list, container, false)
    }

    override fun onStart() {
        super.onStart()
        localEvents.layoutManager = LinearLayoutManager(parentContext)

        viewModel = ViewModelProviders.of(this).get(EventViewModel::class.java)

        val observer = Observer<ArrayList<Event>> {
            localEvents.adapter = adapter
            val result = DiffUtil.calculateDiff(object : DiffUtil.Callback() {
                override fun areItemsTheSame(p0: Int, p1: Int): Boolean {
                    return (eventList[p0].title == eventList[p1].title) and (eventList[p0].location == eventList[p1].location)
                }

                override fun getOldListSize(): Int {
                    return eventList.size
                }

                override fun getNewListSize(): Int {
                    if (it == null) {
                        return 0
                    }
                    return it.size
                }

                override fun areContentsTheSame(p0: Int, p1: Int): Boolean {
                    return eventList[p0] == eventList[p1]
                }
            })
            result.dispatchUpdatesTo(adapter)
            eventList = it ?: ArrayList()
        }

        viewModel.getArtists(queryString).observe(this, observer)
    }

    // Adapter for artist items
    inner class EventAdapter: RecyclerView.Adapter<EventAdapter.EventViewHolder>() {

        override fun onCreateViewHolder(p0: ViewGroup, p1: Int): EventViewHolder {
            val itemView = LayoutInflater.from(p0.context).inflate(R.layout.event_item, p0, false)
            return EventViewHolder(itemView)
        }

        override fun onBindViewHolder(p0: EventViewHolder, p1: Int) {
            val event = eventList[p1]
            p0.title.text = event.title
            p0.location.text =event.description
            if(!event.image.isEmpty()) {
                Picasso.with(this@EventList.parentContext).load(event.image).into(p0.image)
            }

            p0.item.setOnClickListener {
                val fStore: FirebaseFirestore = FirebaseFirestore.getInstance()
                val map:HashMap<String,String> = HashMap<String,String>()
                map["description"] = event.title+"\n"+event.description
                map["user"] = event.eventHost
                fStore.collection("currentEvent").document("C8FOS6QN4nV78iZhgkOn").set(map)
                val intent = Intent(this@EventList.parentContext, EventPage::class.java)
                intent.putExtra("EVENT", event)
                startActivity(intent)
            }
        }

        override fun getItemCount(): Int {
            return eventList.size
        }

        inner class EventViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
            var item = itemView
            var location: TextView = itemView.eventLocation
            var title: TextView = itemView.eventTitle
            var image: ImageView = itemView.eventIcon
        }
    }

}