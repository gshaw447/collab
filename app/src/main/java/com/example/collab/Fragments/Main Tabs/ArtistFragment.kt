package com.example.collab

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.Toast
import kotlinx.android.synthetic.main.fragment_artists.*
import android.widget.TextView


@SuppressLint("ValidFragment")
class ArtistFragment(context: Context): Fragment() {
    private var parentContext = context
    private var initialized: Boolean = false
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_artists, container, false)

    }


    override fun onStart() {
        super.onStart()

        if (!this.initialized) {
            val fm = fragmentManager
            val ft = fm?.beginTransaction()
            //ToDo: Place loaction based artists query
            val localQuery = ""
            ft?.add(R.id.artistListFrag, ArtistList(this.parentContext, localQuery), "NEW_FRAG")
            ft?.commit()

            artistSearch.setOnEditorActionListener { _, actionId, _ ->
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    val searchText = artistSearch.text
                    artistSearch.setText("")
                    if (searchText.toString() == "") {
                        val toast = Toast.makeText(this.parentContext, "Please enter text", Toast.LENGTH_SHORT)
                        toast.setGravity(Gravity.CENTER, 0, 0)
                        toast.show()
                        return@setOnEditorActionListener true
                    }
                    else {
                        performSearch(searchText.toString())
                        return@setOnEditorActionListener false
                    }
                }

                return@setOnEditorActionListener false
            }

            this.initialized = true
        }
    }

    private fun performSearch(query: String) {
        // Load Fragment into View
        val fm = fragmentManager
        val searchQuery = ""
        // add
        val fragment = ArtistList(this.parentContext, searchQuery)
        val ft = fm?.beginTransaction()
        ft?.replace(R.id.artistListFrag, fragment, "RESULTS_FRAG")
        ft?.commit()
    }

}