package com.example.collab

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.Toast
import com.example.collab.Fragments.Lists.CollabList
import kotlinx.android.synthetic.main.fragment_collabs.*

@SuppressLint("ValidFragment")
class CollabFragment(context: Context): Fragment()  {
    private var parentContext = context
    private var initialized: Boolean = false

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_collabs, container, false)
    }

    override fun onStart() {
        super.onStart()

        if (!this.initialized) {
            val fm = fragmentManager
            val ft = fm?.beginTransaction()
            //ToDo: Place location based collabs query
            val localQuery = ""
            ft?.add(R.id.collabListFrag, CollabList(this.parentContext, localQuery), "NEW_FRAG")
            ft?.commit()

            collabSearch.setOnEditorActionListener { _, actionId, _ ->
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    val searchText = collabSearch.text
                    collabSearch.setText("")
                    if (searchText.toString() == "") {
                        val toast = Toast.makeText(this.parentContext, "Please enter text", Toast.LENGTH_SHORT)
                        toast.setGravity(Gravity.CENTER, 0, 0)
                        toast.show()
                        return@setOnEditorActionListener true
                    }
                    else {
                        performSearch(searchText.toString())
                        return@setOnEditorActionListener false
                    }
                }

                return@setOnEditorActionListener false
            }

            this.initialized = true
        }
    }

    private fun performSearch(query: String) {
//        // Load Fragment into View
//        val fm = fragmentManager
//        val searchQuery = "?method=artist.gettoptracks&artist=$query&api_key=27e92ad6b49d20e092bd1ce2b94b87f0&limit=20&format=json"
//        // add
//        val fragment = TrackList(this.parentContext, searchQuery)
//        val ft = fm?.beginTransaction()
//        ft?.replace(R.id.topTrackFrag, fragment, "RESULTS_FRAG")
//        ft?.commit()
    }
}