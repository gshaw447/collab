package com.example.collab.Fragments

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import com.example.collab.R
import kotlinx.android.synthetic.main.fragment_new_event.*
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.auth.FirebaseAuth
import java.util.HashMap

@SuppressLint("ValidFragment")
class NewEventFragment(context: Context): Fragment() {
    private var parentContext = context
    private var initialized: Boolean = false
    private var fAuth:FirebaseAuth? = null
    private var fStore:FirebaseFirestore? = null
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        var view =  inflater.inflate(R.layout.fragment_new_event, container, false)
        fAuth = FirebaseAuth.getInstance()
        fStore = FirebaseFirestore.getInstance()
        var desc: EditText = view.findViewById(R.id.eventDescription)
        var ex: EditText = view.findViewById(R.id.eventDate)
        view.findViewById<Button>(R.id.eventSubmit).setOnClickListener {
            var events = HashMap<String, String>()
            events["description"] = desc.text.toString()
            events["user"] = fAuth?.currentUser?.email.toString()
            events["expr"] = ex.text.toString()
            events["eventmessage"] = ""
            fStore?.collection("events")?.add(events)
        }

        return view
    }
}
