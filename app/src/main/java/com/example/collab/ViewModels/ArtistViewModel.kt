package com.example.collab

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.MutableLiveData
import android.widget.TextView
import com.google.firebase.firestore.FirebaseFirestore
import android.support.v7.app.AppCompatActivity
import com.google.firebase.firestore.QuerySnapshot
class ArtistViewModel(application: Application): AndroidViewModel(application) {
    //Todo
    var fStore:FirebaseFirestore? = null
    private var _artistList: MutableLiveData<ArrayList<Artist>> = MutableLiveData()
//    private var _artistsDBHelper: artistsDatabaseHelper = ArtistsDatabaseHelper(application)


    fun getArtists(query: String): MutableLiveData<ArrayList<Artist>> {
//        loadArtists(query)
        //TEMP Todo:actually set in query
        var temp: ArrayList<Artist> = ArrayList()

        fStore = FirebaseFirestore.getInstance()

        fStore?.collection("users")
            ?.get()
            ?.addOnSuccessListener { result ->
                for (document in result) {
                    var newArtistDoc:Artist = Artist()
                    newArtistDoc.name = document.get("username").toString()
                    newArtistDoc.location = document.get("description").toString()
                    //newArtistDoc.image = document.get("image").toString()

                    temp.add(newArtistDoc)
                }

                _artistList.value = temp
            }
        _artistList.value = temp
        return _artistList
    }
}