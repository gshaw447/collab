package com.example.collab.ViewModels

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.MutableLiveData
import com.example.collab.Classes.Collab
import com.google.firebase.firestore.FirebaseFirestore
class CollabViewModel(application: Application): AndroidViewModel(application)  {
    //Todo
    private var _collabList: MutableLiveData<ArrayList<Collab>> = MutableLiveData()
//    private var _collabsDBHelper: collabsDatabaseHelper = CollabsDatabaseHelper(application)
private var fStore:FirebaseFirestore? = null

    fun getCollabs(query: String): MutableLiveData<ArrayList<Collab>> {
//        loadCollabs(query)
        //TEMP Todo:actually set in query
        var temp: ArrayList<Collab> = ArrayList()
        fStore = FirebaseFirestore.getInstance()

        fStore?.collection("collabs")
            ?.get()
            ?.addOnSuccessListener { result ->
                for (document in result) {
                    var newCollabDoc: Collab = Collab()
                    //newCollabDoc.title = document.get("user").toString()
                    newCollabDoc.title = document.get("description").toString()
                    newCollabDoc.description = "Expires: "+document.get("expr").toString()
                    newCollabDoc.artist.name = document.get("user").toString()
                    temp.add(newCollabDoc)
                }
                _collabList.value = temp
            }
        _collabList.value = temp
        return _collabList
    }
}
