package com.example.collab.ViewModels

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.MutableLiveData
import com.example.collab.Artist
import com.example.collab.Classes.Event
import com.google.firebase.firestore.FirebaseFirestore

class EventViewModel (application: Application): AndroidViewModel(application) {
    //Todo
    private var _eventList: MutableLiveData<ArrayList<Event>> = MutableLiveData()
//    private var _artistsDBHelper: artistsDatabaseHelper = ArtistsDatabaseHelper(application)
private var fStore:FirebaseFirestore? = null


    fun getArtists(query: String): MutableLiveData<ArrayList<Event>> {
//        loadEvents(query)
        //TEMP Todo:actually set in query
        var temp: ArrayList<Event> = ArrayList()
        fStore = FirebaseFirestore.getInstance()

        fStore?.collection("events")
            ?.get()
            ?.addOnSuccessListener { result ->
                for (document in result) {
                    var newEventDoc: Event = Event()
                    newEventDoc.eventHost = document.get("user").toString()
                    newEventDoc.title = document.get("description").toString()
                    newEventDoc.description = "Expires: "+document.get("expr").toString()
                    temp.add(newEventDoc)
                }
                _eventList.value = temp
            }
        _eventList.value = temp
        return _eventList
    }
}